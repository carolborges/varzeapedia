<?php
/**
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'varzeapedia');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|YDjWkh76zf!sK$X`kT %@#=BGJz+0/7WUK+.Y`)l3Bo9~r)7^+LR@KNAF;K._F)');
define('SECURE_AUTH_KEY',  'Eb|;g$o-&&[Jjo^P+%(LslU,!-^+&~&A^FFH#9J,=e{j6~?f0<r-NX?MwSFR;s-6');
define('LOGGED_IN_KEY',    'zzCyjDR-u$chziOQ+.%@9Us4lmCI;-Nf51mjB0y8J|2;b4im> G]d!;?H(Db|!RY');
define('NONCE_KEY',        'a55)0i~|22v/VVrKnL%GJ2+(<7Kh+&TCTyU-o^m-wYD=TU2KL?qb;K+wGg- J+]A');
define('AUTH_SALT',        'XqZ[bxs/F2u?z|J1U=.akz/rA7E*7m-hdkilq`+0MmoVxKdbQ|py0WXD| 0d?81B');
define('SECURE_AUTH_SALT', 'DCM:#|^]sa;|?sZPo.z[Vw?C9&~R8YGEd z1@C2qb8&B`8h0EufSlmjCr9hspnVX');
define('LOGGED_IN_SALT',   'U~mrGH(*_4[CGW~ oZPVOnIP=-/l+q7d]H<?o3{,g~i%+Mr.|$.U8^P+KTxz-@N_');
define('NONCE_SALT',       'Ld?s*[S+P`}j |u!jq|9n|n1Y&+#m$z&9d4{Cd-R~[*3[q2Z,)!Er7JJeU_a&Wy.');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
