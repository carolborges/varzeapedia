<?php get_header(); ?>
<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>

            <div class="container">
                <div class="col-md-9 pull-left box-titulo-pagina">
                    <h1><?php echo $term->name; ?></h1>
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                        <?php
                            $url.= $_SERVER['REQUEST_URI'];
                            $link = explode('/', $url);
                         ?>
                         <li>
                          <a href="<?php echo get_site_url();  ?>/<?php  echo $link[2];?>">
                           <?php  echo ucfirst($link[2]);?>s
                          </a>
                        </li>
                        <li class="active"><?php echo $term->name; ?></li>
                    </ol>
                    <div class="col-md-12 box-conteudo-pagina">
                        <ul class="listagem-item">
                          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                            <li class="col-md-6 pull-left">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </li>
                          <?php endwhile; ?>

                        </ul>
                      <nav>
                        <ul class="pagination">
                          <li><?php if (function_exists('pagination_funtion')) pagination_funtion(); ?> </li>
                        </ul>
                      </nav>
                    </div>
                </div>
                <div class="col-md-3 pull-right sidebar">
                    <h6>LEIA TAMBÉM</h6>
                    <ul class="lista-leia-tbm">
                        <?php  $args =  array('post_type' => 'noticias', 'posts_per_page'=>12, 'order'=>'DESC');
                         query_posts( $args );
                        ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                        <li>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>
                         <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
<?php get_footer(); ?>
