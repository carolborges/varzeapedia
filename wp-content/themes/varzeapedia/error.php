<?php
/**
 * page error
 */
?>
            <div class="container">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                      <li class="active"><?php the_title(); ?></li>
                    </ol>
                </div>
            </div>
            <div class="container">
                <div class="col-md-9 pull-left box-titulo-pagina">
                    <span>Você buscou por: Termo de busca</span>
                    <div class="col-md-12 box-conteudo-pagina">
        			<h2><?php _e('Página não encontrada', 'varzeapedia'); ?></h2>

		       			<?php
							if (is_search()) {
								echo '<p>Nenhum post encontrado. Tente realizar uma pesquisa diferente.</p>';
							} else {
								echo '<p>' . __('Tente refazer sua pesquisa.', 'varzeapedia') . '</p>';
							}
						?>
		                    </div>
                </div>
                <div class="col-md-3 pull-right sidebar">
                    <h6>LEIA TAMBÉM</h6>
                    <ul class="lista-leia-tbm">
                        <?php  $args =  array('post_type' => 'campos', 'posts_per_page'=>12, 'order'=>'DESC');
                         query_posts( $args );
                        ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                        <li>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>

                         <?php endwhile; ?>

                    </ul>
                </div>
            </div>
        </div>
