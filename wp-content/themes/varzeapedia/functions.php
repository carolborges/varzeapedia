<?php

	add_theme_support('menus');

	register_nav_menu('navbar', 'Navegação Topo');

	add_theme_support( 'post-thumbnails' );

  require_once ( 'posts-types/taxonomia_destaque.php' );
  require_once ( 'posts-types/taxonomia_campeonato.php' );
  require_once ( 'posts-types/taxonomia_campo.php' );
	require_once ( 'posts-types/taxonomia_equipe.php' );
	require_once ( 'posts-types/noticias.php' );
  require_once ( 'posts-types/save_post.php' );
  require_once ( 'posts-types/comentarios.php' );
  require_once ( 'posts-types/fotos.php' );
  require_once ( 'posts-types/radio.php' );
  require_once ( 'posts-types/video.php' );
  require_once ( 'posts-types/campeonatos.php' );
  require_once ( 'posts-types/campos.php' );
  require_once ( 'posts-types/equipes.php' );
	require_once ( 'posts-types/midias.php' );

  require_once('wp_bootstrap_navwalker.php');



	function limitarTexto($texto, $limite)
	{

	  if(strlen($texto) >= (int) $limite)
	    $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';

	  return $texto;

	}

/** Paginação */
function pagination_funtion() {

global $wp_query;
$total = $wp_query->max_num_pages;

if ( $total > 1 )  {
    if ( !$current_page = get_query_var('paged') )
        $current_page = 1;

        $big = 999999999;

        $permalink_structure = get_option('permalink_structure');
        $format = empty( $permalink_structure ) ? '&page=%#%' : 'page/%#%/';
        echo paginate_links(array(
            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
            'format' => $format,
            'current' => $current_page,
            'total' => $total,
            'mid_size' => 4,
            'type' => 'plain'
        ));
    }

}
/** END Paginação */
// $tipo aceita: default, 0, 1, 2 e 3 como parâmetro
function imagemYouTube($url, $tipo = 0){
  // cria um array com várias informações da url
  $yt_link = parse_url($url);
  // verifica se no array existe a chave query
  // e verifica se o parâmetro $tipo é aceito
  if (isset($yt_link['query']) && in_array($tipo, array(0,1,2,3,'default'))){
    // transforma a querystring em um array associativo
    parse_str($yt_link['query'], $video);
    // verifica se tem a chave v no array $video
    if (isset($video['v'])){
      // retorna o link da imagem de acordo com o tipo
      return 'http://i1.ytimg.com/vi/'.$video['v'].'/'.$tipo.'.jpg';
    }
  }
  // retorna false. Pode ser trocado por um link
  // de uma imagem padrão
  // use como abaixo para isso
  // return 'http://endereco-da-imagem-padrao/imagem.jpg';
  return false;
}


  /* Theme setup */
  add_action( 'after_setup_theme', 'wpt_setup' );
      if ( ! function_exists( 'wpt_setup' ) ):
          function wpt_setup() {
              register_nav_menu( 'pull-left', __( 'Menu Esquerdo', 'wptuts' ) );
              register_nav_menu( 'pull-right', __( 'Menu Direito', 'wptuts' ) );
          }
      endif;



  function wpt_register_js() {
      wp_register_script('jquery.bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', 'jquery');
      wp_enqueue_script('jquery.bootstrap.min');
  }
  add_action( 'init', 'wpt_register_js' );
  function wpt_register_css() {
      wp_register_style( 'bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css' );
      wp_enqueue_style( 'bootstrap.min' );
  }
  add_action( 'wp_enqueue_scripts', 'wpt_register_css' );


 ?>
