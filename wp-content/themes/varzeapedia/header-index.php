<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!--[if lt IE 9]>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
  <![endif]-->
  <script>(function(){document.documentElement.className='js'})();</script>
  <?php wp_head(); ?>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta http-equiv="Content-Style-Type" content="text/css">

        <meta name="description" content="VárzeaPédia é um site sobre a história do futebol de várzea, fundação dos times amadores do Brasil em qualquer quebrada e bairro. Aqui é papo de boleiro varzeano!">
        <meta name="keywords" content="futebol de várzea, pelada, varzeano, futebol amador, futebol, várzea, boleiro, pesquisa, história, times, bairro, quebrada, periferia, torcida, campos, campos de varzea, varzea, futebol de varzea">
        <meta property="og:image" content="<?php bloginfo("template_url");?>/img/og-img.jpg"/>
        <meta name="robots" content="index,follow">
        <meta name="author" content="carolborges.me">
        <meta name="copyright" content="Varzeapédia">
        <base href="www.varzeapedia.com">

        <title>VarzeaPédia - A enciclopédia do futebol amador</title>

        <link rel="shortcut icon" href="<?php bloginfo("template_url");?>/img/favicon.png" />
        <link rel="canonical" href="http://www.www.varzeapedia.com" />

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link href="<?php bloginfo("template_url");?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php bloginfo("template_url");?>/css/estilo.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

        <script src="<?php bloginfo("template_url");?>/js/bootstrap.min.js"></script>
        <script src="<?php bloginfo("template_url");?>/js/html5lightbox.js"></script>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-38441887-3', 'auto');
          ga('send', 'pageview');

        </script>

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <script>window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));</script>

    </head>

    <body>
      <div class="row topo-background">
        <div class="container-fluid">
          <div class="col-md-12">
            <ul class="lista-link-midias">
              <li><a href="https://pt-br.facebook.com/VarzeaPedia" target="_blank" class="ico-facebook"><span class="fa fa-facebook-square"></span></a></li>
              <li><a href="https://twitter.com/VarzeaPedia" target="_blank" class="ico-twitter"><span class="fa fa-twitter-square"></span></a></li>
              <li><a href="https://www.instagram.com/varzeapedia/" target="_blank" class="ico-instagram"><span class="fa fa-instagram"></span></a></li>
            </ul>
            <a href="<?php echo get_site_url(); ?>" class="logo" id="logo-index"><img src="<?php bloginfo("template_url");?>/img/varzeapedia.png" alt="" />varzeapedia</a>
          </div>
          </div>
      </div>
    <div class="row bg-menu">
      <div class="container-fluid">
        <div class="container">
            <div class="col-md-12 box-menu">

                    <?php
                        wp_nav_menu( array(
                          'menu' => 'Menu Topo Esq',
                          'depth' => 2,
                          'container' => false,
                          'menu_class' => 'menu-nav pull-left',
                          //Process nav menu using our custom nav walker
                          'walker' => new wp_bootstrap_navwalker())
                        );
                    ?>

                   <?php
                        //wp_nav_menu( array(
                          //'menu' => 'Menu Topo Direito',
                          //'depth' => 2,
                          //'container' => false,
                          //'menu_class' => 'menu-nav pull-left',
                          //Process nav menu using our custom nav walker
                          //'walker' => new wp_bootstrap_navwalker())
                        //);
                    ?>

                    <form class="pull-right form-search" role="search">
                        <div class="form-group group-search">
                        <?php get_search_form(); ?>
                        </div>
                    </form>

            </div>
        </div>
      </div>
    </div>
