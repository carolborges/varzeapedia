<?php include ("header-index.php"); ?>
            <div class="container">
                <div class="col-md-12">
                    <ul class="lista-destaques">
                      <?php  $args =  array('post_type' => 'equipes', 'tax_query'=>array(array('taxonomy'=>'destaque', 'field'=>'slug', 'terms'=> array('capa'), 'operator'=>'IN', 'include_children '=>true)), 'posts_per_page'=>6, 'order'=>'DESC');
                       query_posts( $args );
                      ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                        <li class="col-xs-12 col-sm-2 col-md-4"> 
                            <div>
                                <i class="bullet-equipes">Equipes</i>
                                <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" width="330" height="170" >
                                <h2><?php the_title(); ?></h2>
                                <p><?php the_excerpt(); ?></p>
                                <a href="<?php the_permalink(); ?>">continue lendo...</a>
                            </div>
                        </li>
                     <?php endwhile; ?>
                    </ul>

                    <ul class="lista-destaques">
                      <?php  $args =  array('post_type' => 'noticias', 'tax_query'=>array(array('taxonomy'=>'destaque', 'field'=>'slug', 'terms'=> array('capa'), 'operator'=>'IN', 'include_children '=>true)), 'posts_per_page'=>3, 'order'=>'DESC');
                       query_posts( $args );
                      ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                        <li class="col-xs-12 col-sm-2 col-md-4">
                            <div>
                                <i class="bullet-noticias">Notícia</i>
                                <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" width="330" height="170" >
                                <h2><?php the_title(); ?></h2>
                                <p><?php the_excerpt(); ?></p>
                                <a href="<?php the_permalink(); ?>">continue lendo...</a>
                            </div>
                        </li>
                     <?php endwhile; ?>
                    </ul>

                    <ul class="lista-destaques">
                      <?php  $args =  array('post_type' => 'campeonatos', 'tax_query'=>array(array('taxonomy'=>'destaque', 'field'=>'slug', 'terms'=> array('capa'), 'operator'=>'IN', 'include_children '=>true)), 'posts_per_page'=>3, 'order'=>'DESC');
                       query_posts( $args );
                      ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                        <li class="col-xs-12 col-sm-2 col-md-4">
                            <div>
                                <i class="bullet-campeonatos">Campeonatos</i>
                                <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" width="330" height="170" >
                                <h2><?php the_title(); ?></h2>
                                <p><?php the_excerpt(); ?></p>
                                <a href="<?php the_permalink(); ?>">continue lendo...</a>
                            </div>
                        </li>
                     <?php endwhile; ?>
                    </ul>

                    <ul class="lista-destaques">
                      <?php  $args =  array('post_type' => 'campos', 'tax_query'=>array(array('taxonomy'=>'destaque', 'field'=>'slug', 'terms'=> array('capa'), 'operator'=>'IN', 'include_children '=>true)), 'posts_per_page'=>3, 'order'=>'DESC');
                       query_posts( $args );
                      ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                        <li class="col-xs-12 col-sm-2 col-md-4">
                            <div>
                                <i class="bullet-campos">Campos</i>
                                <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" width="330" height="170" >
                                <h2><?php the_title(); ?></h2>
                                <p><?php the_excerpt(); ?></p>
                                <a href="<?php the_permalink(); ?>">continue lendo...</a>
                            </div>
                        </li>
                     <?php endwhile; ?>
                    </ul>

                </div>
            </div>
            <div class="row bg-depoimentos">
            <div class="container">
                <div class="col-md-12">
                    <h3>A rapaziada fala...</h3>
                    <ul class="lista-depoimentos">
                      <?php  $args =  array('post_type' => 'comentarios', 'posts_per_page'=>1, 'order'=>'DESC');
                       query_posts( $args );
                      ?>
                       <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                        <li>
                            <div>
                                <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>">
                                <?php the_content(); ?>
                                <span><?php the_title(); ?>, <?php the_time('Y'); ?></span>
                            </div>
                        </li>
                      <?php endwhile; ?>
                    </ul>
                </div>
            </div>
          </div>

          <div class="container facebook-post">
              <div class="col-md-12">
                <h4>Tem tbm Facebook...</h4>
                <div class="fb-like-box" data-href="https://www.facebook.com/VarzeaPedia" data-width="520" data-height="600" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="true" data-show-border="true"></div>
              </div>
          </div>

          <div class="row twitter-post">
            <div class="container">
                <div class="col-md-12">
                  <h4>E olha o twitter ai!</h4>
                  <div class="twitter-post-widget">
                    <a class="twitter-timeline" width="800" height="600"  href="https://twitter.com/VarzeaPedia" data-widget-id="593792323890348032">Tweets de @VarzeaPedia</a>
                  </div>
                </div>
            </div>
          </div>

        </div>

<?php get_footer(); ?>
