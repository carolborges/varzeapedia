<?php
/*
 * Template Name: Equipes
 */

 ?>
<?php get_header(); ?>
            <div class="container">
                <div class="col-md-9 pull-left box-titulo-pagina">
                    <h1><?php the_title(); ?></h1>
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                      <li class="active"><?php the_title(); ?></li>
                    </ol>
                    <div class="col-md-12 box-conteudo-pagina">
                        <ul class="listagem-item">
                          <?php  query_posts('post_type=equipes') ?>
                          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                            <li class="col-md-6 pull-left">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </li>
                          <?php endwhile; ?>

                        </ul>
                      <nav>
                        <ul class="pagination">
                          <li><?php if (function_exists('pagination_funtion')) pagination_funtion(); ?> </li>
                        </ul>
                      </nav>
                    </div>
                </div>
                <div class="col-md-3 pull-right sidebar">
                    <h6>LEIA TAMBÉM</h6>
                    <ul class="lista-leia-tbm">
                      <?php  query_posts('post_type=noticias') ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                        <li>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>
                         <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>


<?php get_footer(); ?>
