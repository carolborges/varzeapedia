<?php
/*
 * Template Name: Galeria Fotos
 */

 ?>
<?php get_header(); ?>
            <div class="container">
                <div class="col-md-12 box-titulo-pagina">
                    <h1>Galeria de Foto</h1>
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                      <li class="active"><?php the_title(); ?></li>
                    </ol>
                    <div class="col-md-12 box-conteudo-pagina">
                        <ul class="lista-foto">
                        <?php  $args =  array('post_type' => 'fotos', 'posts_per_page'=>12, 'order'=>'DESC');
                         query_posts( $args );
                        ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                          <li class="col-md-3">
                              <a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>">
                                  <img src='<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),array(308, 308)));?>' width="240" height="120" >
                                 <h2><?php// the_title(); ?></h2>
                                  <p><?php// the_excerpt(); ?></p>
                              </a>
                          </li>

                          <?php endwhile; ?>
                          </ul>
                        <nav>
                          <ul class="pagination">
                            <li><?php if (function_exists('pagination_funtion')) pagination_funtion(); ?> </li>
                          </ul>
                        </nav>
                    </div>
                </div>
            </div>

<?php get_footer(); ?>
