<?php
/*
 * Template Name: Galeria Radio
 */

 ?>
<?php get_header(); ?>
            <div class="container">
                <div class="col-md-12 box-titulo-pagina">
                    <h1>Galeria de Rádio</h1>
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                      <li class="active"><?php the_title(); ?></li>
                    </ol>
                    <div class="col-md-12 box-conteudo-pagina">
                        <ul class="lista-radio">
                        <?php  $args =  array('post_type' => 'radio', 'posts_per_page'=>4, 'order'=>'DESC');
                         query_posts( $args );
                        ?>
                          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                            <li class="col-md-6">
                                <p><?php the_title(); ?></p>
                                <?php the_content(); ?>
                            </li>
                          <?php endwhile; ?>
                        </ul>
                        <nav>
                          <ul class="pagination">
                            <li><?php if (function_exists('pagination_funtion')) pagination_funtion(); ?> </li>
                          </ul>
                        </nav>
                    </div>
                </div>
            </div>

<?php get_footer(); ?>
