<?php
/*
 * Template Name: Galeria Videos
 */

 ?>
<?php get_header(); ?>
            <div class="container">
                <div class="col-md-12 box-titulo-pagina">
                    <h1><?php the_title(); ?></h1>
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                      <li class="active"><?php the_title(); ?></li>
                    </ol>
                    <div class="col-md-12 box-conteudo-pagina">
                        <ul class="lista-video">
                      <?php  $args =  array('post_type' => 'video', 'posts_per_page'=>8, 'order'=>'DESC');
                       query_posts( $args );
                      ?>
                          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                           <li class="pull-left">
                              <a href="<?php echo get_post_meta($post->ID, 'link', true); ?>" class="html5lightbox"  title="<?php the_title(); ?>">
                                  <?php
                                    $linkYoutube = get_post_meta($post->ID, 'link', true);
                                   ?>
                                  <img src="<?php echo imagemYouTube("$linkYoutube", '0')?>" />
                                  <h2><?php the_title(); ?></h2>
                                  <p><?php the_excerpt(); ?></p>
                                </a>

                            </li>
                          <?php endwhile; ?>
                        </ul>
                        <nav>
                          <ul class="pagination">
                            <li><?php if (function_exists('pagination_funtion')) pagination_funtion(); ?> </li>
                          </ul>
                        </nav>
                    </div>
                </div>
            </div>

<?php get_footer(); ?>
