<?php
/*
 * Template Name: Institucional
 */

 ?>

<?php get_header(); ?>
            <div class="container">
                <div class="col-md-12 box-titulo-pagina">
                     <?php if (have_posts())  : the_post(); ?>
                    <h1><?php the_title(); ?></h1>
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                      <li class="active"><?php the_title(); ?></li>
                    </ol>
                    <div class="col-md-12 box-conteudo-pagina">
                      <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" class="pull-right">
                     <?php the_content(); ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>

<?php get_footer(); ?>
