<?php
/*
 * Template Name: Listagem Galerias
 */

 ?>
<?php get_header(); ?>

            <div class="container">
                <div class="col-md-12 box-titulo-pagina galeria-videos-page">
                    <div class="col-md-12">
                        <ul class="lista-galerias">
                          <?php wp_reset_query(); query_posts('page_id=29') ?>
                          <?php if ( have_posts() ) : the_post(); ?>
                            <li class="pull-left">
                                 <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>">
                                <h2><?php the_title(); ?></h2>
                                <a href="<?php the_permalink(); ?>">veja mais</a>
                            </li>
                          <?php endif; ?>

                          <?php wp_reset_query(); query_posts('page_id=31') ?>
                          <?php if ( have_posts() ) : the_post(); ?>
                            <li class="pull-left">
                                 <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>">
                                <h2><?php the_title(); ?></h2>
                                <a href="<?php the_permalink(); ?>">veja mais</a>
                            </li>
                          <?php endif; ?>

                          <?php wp_reset_query(); query_posts('page_id=33') ?>
                          <?php if ( have_posts() ) : the_post(); ?>
                            <li class="pull-left">
                                 <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>">
                                <h2><?php the_title(); ?></h2>
                                <a href="<?php the_permalink(); ?>">veja mais</a>
                            </li>
                          <?php endif; ?>

                        </ul>
                    </div>
                </div>
            </div>


<?php get_footer(); ?>
