<?php
/*
 * Template Name: Midias
 */

 ?>
<?php get_header(); ?>

            <div class="container">
                <div class="col-md-9 pull-left box-titulo-pagina">
                    <h1><?php the_title(); ?></h1>
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                      <li class="active"><?php the_title(); ?></li>
                    </ol>
                    <div class="col-md-12 box-conteudo-pagina">
                        <ul class="lista-midia">
                        <?php  $args =  array('post_type' => 'midias', 'posts_per_page'=>10, 'order'=>'DESC');
                         query_posts( $args );
                        ?>
                          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                            <li class="pull-left">
                              <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" width="330px;" heigth="170px;" class="pull-right">
                                <h2><?php the_title(); ?></h2>
                                <a href="<?php the_permalink(); ?>">continue vendo...</a>
                            </li>

                          <?php endwhile; ?>

                        </ul>
                      <nav>
                        <ul class="pagination">
                          <li><?php if (function_exists('pagination_funtion')) pagination_funtion(); ?> </li>
                        </ul>
                      </nav>
                    </div>
                </div>
                <div class="col-md-3 pull-right sidebar">
                    <h6>LEIA TAMBÉM</h6>
                    <ul class="lista-leia-tbm">
                        <?php  $args =  array('post_type' => 'noticias', 'posts_per_page'=>12, 'order'=>'DESC');
                         query_posts( $args );
                        ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                        <li>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>
                         <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>


<?php get_footer(); ?>
