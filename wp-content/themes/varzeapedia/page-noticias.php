<?php
/*
 * Template Name: Noticias
 */

 ?>
<?php get_header(); ?>
            <div class="container">
                <div class="col-md-12 box-titulo-pagina">
                    <h1>Notícias</h1>
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                      <li class="active"><?php the_title(); ?></li>
                    </ol>
                    <div>
                        <ul class="lista-destaques">
                        <?php  $args =  array('post_type' => 'noticias', 'posts_per_page'=>10, 'order'=>'DESC');
                         query_posts( $args );
                        ?>
                          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                            <li class="col-xs-12 col-sm-2 col-md-4">
                              <div>
                                <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" >
                                  <h2><?php the_title(); ?></h2>
                                  <p><?php the_excerpt(); ?></p>
                                  <a href="<?php the_permalink(); ?>">continue lendo</a>
                              </div>
                            </li>
                          <?php endwhile; ?>
                        </ul>
                    </div>
                      <nav>
                        <ul class="pagination">
                          <li><?php if (function_exists('pagination_funtion')) pagination_funtion(); ?> </li>
                        </ul>
                      </nav>
                </div>
            </div>
        </div>
<?php get_footer(); ?>
