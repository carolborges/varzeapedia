<?php

add_action('init', 'campeonatos_register');

function campeonatos_register() {

	$labels = array(
			'name' => __('Campeonatos'),
			'singular_name' => __('campeonatos'),
			'add_new' => __('Novo Campeonato'),
			'add_new_item' => __('Adicionar novo Campeonato'),
			'edit_item' => __('Editar Campeonato'),
			'new_item' => __('Novo Campeonato'),
			'view_item' => __('Ver Campeonato'),
			'search_items' => __('Buscar Campeonato'),
			'not_found' =>  __('Nenhuma Campeonato encontrado '),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>5,
			'taxonomies'=>array( 'destaques',),
			'supports'=>array('title', 'editor','thumbnail','excerpt')
			

	);

	register_post_type( 'campeonatos' , $args );
	
}


