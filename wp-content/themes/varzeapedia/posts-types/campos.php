<?php

add_action('init', 'campos_register');

function campos_register() {

	$labels = array(
			'name' => __('Campos'),
			'singular_name' => __('campos'),
			'add_new' => __('Novo Campo'),
			'add_new_item' => __('Adicionar novo Campo'),
			'edit_item' => __('Editar Campo'),
			'new_item' => __('Novo Campo'),
			'view_item' => __('Ver Campo'),
			'search_items' => __('Buscar Campo'),
			'not_found' =>  __('Nenhuma Campo encontrado '),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>6,
			'taxonomies'=>array( 'destaques',),
			'supports'=>array('title', 'editor','thumbnail')


	);

	register_post_type( 'campos' , $args );

}
