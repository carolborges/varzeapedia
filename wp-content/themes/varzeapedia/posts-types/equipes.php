<?php

add_action('init', 'equipes_register');

function equipes_register() {

	$labels = array(
			'name' => __('Equipes'),
			'singular_name' => __('equipes'),
			'add_new' => __('Nova Equipe'),
			'add_new_item' => __('Adicionar nova Equipe'),
			'edit_item' => __('Editar Equipe'),
			'new_item' => __('Nova Equipe'),
			'view_item' => __('Ver Servico'),
			'search_items' => __('Buscar Equipe'),
			'not_found' =>  __('Nenhuma Equipe encontrada'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>5,
			'taxonomies'=>array( 'destaques',),
			'supports'=>array('title', 'editor','thumbnail','excerpt')


	);

	register_post_type( 'equipes' , $args );

}
