<?php

add_action('init', 'fotos_register');

function fotos_register() {

	$labels = array(
			'name' => __('Galeria de Fotos'),
			'singular_name' => __('fotos'),
			'add_new' => __('Nova Galeria de Fotos'),
			'add_new_item' => __('Adicionar nova Galeria de Fotos'),
			'edit_item' => __('Editar Galeria de Fotos'),
			'new_item' => __('Nova Galeria de Fotos'),
			'view_item' => __('Ver Galeria de Fotos'),
			'search_items' => __('Buscar Galeria de Fotos'),
			'not_found' =>  __('Nenhuma Galeria de Fotos encontrada'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>4,
			'supports'=>array('title', 'excerpt', 'thumbnail')
			

	);

	register_post_type( 'fotos' , $args );
	
}


