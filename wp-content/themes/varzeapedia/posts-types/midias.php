<?php

add_action('init', 'midias_register');

function midias_register() {

	$labels = array(
			'name' => __('Na Midia'),
			'singular_name' => __('midias'),
			'add_new' => __('Nova Midia'),
			'add_new_item' => __('Adicionar novo Midia'),
			'edit_item' => __('Editar Midia'),
			'new_item' => __('Novo Midia'),
			'view_item' => __('Ver Midia'),
			'search_items' => __('Buscar Midia'),
			'not_found' =>  __('Nenhuma Midia encontrado '),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>7,
			'supports'=>array('title', 'editor', 'thumbnail')
			

	);

	register_post_type( 'midias' , $args );
	
}


