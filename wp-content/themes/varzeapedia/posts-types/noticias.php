<?php

add_action('init', 'noticias_register');

function noticias_register() {

	$labels = array(
			'name' => __('Noticias'),
			'singular_name' => __('noticia'),
			'add_new' => __('Nova Noticia'),
			'add_new_item' => __('Adicionar nova Noticia'),
			'edit_item' => __('Editar Noticia'),
			'new_item' => __('Nova Noticia'),
			'view_item' => __('Ver Servico'),
			'search_items' => __('Buscar Noticia'),
			'not_found' =>  __('Nenhuma Noticia encontrada'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>3,
			'taxonomies'=>array( 'destaques',),
			'supports'=>array('title', 'editor', 'thumbnail','excerpt')
			

	);

	register_post_type( 'noticias' , $args );
	
}


