<?php

add_action('init', 'radio_register');

function radio_register() {

	$labels = array(
			'name' => __('Radio'),
			'singular_name' => __('radio'),
			'add_new' => __('Novo Radio'),
			'add_new_item' => __('Adicionar novo Radio'),
			'edit_item' => __('Editar Radio'),
			'new_item' => __('Novo Radio'),
			'view_item' => __('Ver Radio'),
			'search_items' => __('Buscar Radio'),
			'not_found' =>  __('Nenhuma Radio encontrada'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>5,
			'supports'=>array('title', 'editor')
			

	);

	register_post_type( 'radio' , $args );
	
}


