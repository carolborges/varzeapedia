<?php
add_action('init', 'campeonato_register');

function campeonato_register(){
	
	$argsCategoria = array(
				
			'labels'=>array(
					'name' => __('Cidades do Campeonato'),
					'singular_name' => __('campeonato'),
					'add_new' => __('Nova Cidade'),
					'add_new_item' => __('Adicionar nova Cidade'),
					'edit_item' => __('Editar Cidade'),
					'new_item' => __('Novo Cidade'),
					'view_item' => __('Ver Cidade'),
					'search_items' => __('Buscar Cidade'),
			),
			'hierarchical'=>true,


	);

	register_taxonomy('campeonato', array('campeonatos'), $argsCategoria);
}