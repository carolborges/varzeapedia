<?php
add_action('init', 'campo_register');

function campo_register(){
	
	$argsCategoria = array(
				
			'labels'=>array(
					'name' => __('Cidades do Campos'),
					'singular_name' => __('campo'),
					'add_new' => __('Nova Cidade'),
					'add_new_item' => __('Adicionar nova Cidade'),
					'edit_item' => __('Editar Cidade'),
					'new_item' => __('Novo Cidade'),
					'view_item' => __('Ver Cidade'),
					'search_items' => __('Buscar Cidade'),
			),
			'hierarchical'=>true,


	);

	register_taxonomy('campo', array('campos'), $argsCategoria);
}