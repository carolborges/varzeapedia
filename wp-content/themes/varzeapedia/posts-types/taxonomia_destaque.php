<?php
add_action('init', 'destaque_register');

function destaque_register(){

	$argsCategoria = array(

			'labels'=>array(
					'name' => __('Destaques'),
					'singular_name' => __('Destaque'),
					'add_new' => __('Novo Destaque'),
					'add_new_item' => __('Adicionar novo Destaque'),
					'edit_item' => __('Editar Destaque'),
					'new_item' => __('Novo Destaque'),
					'view_item' => __('Ver Destaque'),
					'search_items' => __('Buscar Destaque'),
			),
			'hierarchical'=>true,


	);

	register_taxonomy('destaque', array( 'noticias' , 'campeonatos', 'campos', 'equipes'), $argsCategoria);
}
