<?php
add_action('init', 'regiao_register');

function regiao_register(){
	
	$argsCategoria = array(
				
			'labels'=>array(
					'name' => __('Cidades'),
					'singular_name' => __('regiao'),
					'add_new' => __('Nova Cidade'),
					'add_new_item' => __('Adicionar nova Cidade'),
					'edit_item' => __('Editar Cidade'),
					'new_item' => __('Novo Cidade'),
					'view_item' => __('Ver Cidade'),
					'search_items' => __('Buscar Cidade'),
			),
			'hierarchical'=>true,


	);

	register_taxonomy('regiao', array('campos' , 'campeonatos' , 'equipes'), $argsCategoria);
}