<?php

add_action('init', 'video_register');

function  video_register() {

	$labels = array(
			'name' => __('Videos'),
			'singular_name' => __('video'),
			'add_new' => __('Novo Video'),
			'add_new_item' => __('Adicionar  Video'),
			'edit_item' => __('Editar  Video'),
			'new_item' => __('Novo  Video'),
			'view_item' => __('Ver  Video'),
			'search_items' => __('Buscar Video'),
			'not_found' =>  __('Nenhum  Video encontrado'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>4,
			// 'taxonomies'=>array('departamentos', 'destaques', 'tipo_produto'),
			'register_meta_box_cb'=> 'dados_video',
			'supports'=>array('title', 'excerpt')
			

	);

	register_post_type( 'video' , $args );
	
}

function dados_video(){
	
	
	add_meta_box("detalhe_video", "Dados Video", "detalhe_video", "video", "normal", "low");

}


function detalhe_video(){
	
	$meta_post = get_post_custom($post->ID);
	
	$link 		 		= $meta_post["link"] [0];

	?>
			

	<div class="conteudo" style="width: 700px; height: 100px;">
	    <div class="esquerdo" style=" float: left;">
		    
		    <label style=" width: 600px; font-weight: bold; padding-top: 30px;">Link do Video:</label><br>
			<input style=" width: 600px; " name="meta_post[link]"  value="<?php echo $link; ?>"><br><br>

	    </div>   

	</div>	 
	  		
	<?php	
}


