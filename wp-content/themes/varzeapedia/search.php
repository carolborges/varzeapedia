<?php
/**
* Template Name: Search Form
**/



get_header(); ?>


	<?php if (have_posts()) : ?>
            <div class="container">
                <div class="col-md-12">

                </div>
            </div>
            <div class="container">
                <div class="col-md-9 pull-left box-titulo-pagina">
                    <span> <?php _e('Você buscou por: ', 'varzeapedia'); ?><?php the_search_query(); ?></span>
										<ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                      <li class="active"><?php the_search_query(); ?></li>
                    </ol>
                    <div class="col-md-12 box-conteudo-pagina">
                        <ul class="lista-resultado">

                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                            <li>
                                <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                                <p><a href="<?php the_permalink() ?>"><?php echo limitarTexto($post->post_content, 180);?>[...]</a></p>
                            </li>
							<?php endwhile; ?>

                        </ul>
                        <nav>

                          <ul class="pagination">

                            <li><?php if (function_exists('pagination_funtion')) pagination_funtion(); ?> </li>

                          </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-3 pull-right sidebar">
                    <h6>LEIA TAMBÉM</h6>
                    <ul class="lista-leia-tbm">
                        <?php  $args =  array('post_type' => 'noticias', 'posts_per_page'=>12, 'order'=>'DESC');
                         query_posts( $args );
                        ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                        <li>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>

                         <?php endwhile; ?>

                    </ul>
                </div>
            </div>
        </div>
        <?php else : ?>

		<?php get_template_part('error'); ?>

		<?php endif; ?>


<?php get_footer(); ?>
