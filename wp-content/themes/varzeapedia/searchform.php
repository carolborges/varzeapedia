
     
<?php
/**
 * Busca Varzea Pedia
 *
 */
?>


<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="searcharea">
     <input type="text" class="col-md-10 input-search"  value="<?php the_search_query(); ?>" name="s" id="s"  placeholder="<?php _e('pesquise aqui...', THE_LANG );?>" />
        <button type="submit" class="col-md-2 btn-search" value="<?php the_search_query(); ?>" ><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
    </div>
</form>

