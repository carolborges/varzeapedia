<?php get_header(); ?>
            <div class="container">
                <div class="col-md-12 box-titulo-pagina">
                    <h1>Galeria de Fotos</h1>
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a>s</li>
                      <li>
                          <a href="<?php echo get_site_url();  ?>/galeria-de-fotos">
                           <?php $nomeTipoPost = get_post_type_object( 'fotos' );
                            echo $nomeTipoPost->labels->name; ?>
                         </a>
                     </li>
                      <li class="active"><?php the_title(); ?></li>
                    </ol>
                    <div class="col-md-12 box-conteudo-pagina">
                        <ul class="lista-foto">
                             <?php
                                $args = array(
                                    'post_type' => 'attachment',
                                    'numberposts' => -1,
                                    'post_status' => null,
                                    'post_parent' => $post->ID,
                                  'exclude'=>get_post_thumbnail_id(get_the_ID())
                                );
                                $attachments = get_posts( $args );

                                if ( $attachments ) { foreach ($attachments as $attachment) { $link = wp_get_attachment_image_src($attachment->ID,array(308, 308)); ?>
                              <li class="col-md-3">
                                  <a href="<?php echo $link[0]; ?>" class="html5lightbox" data-group="mygroup" title="<?php the_title(); ?>">
                                      <img src='<?php echo $link[0]; ?>' width="240" height="120" >
                                  </a>
                              </li>

                              <?php } } ?>

                          </ul>
                        <nav>
                          <!-- <ul class="pagination"> -->
                            <li><?php// if (function_exists('pagination_funtion')) pagination_funtion(); ?> </li>
                          <!-- </ul> -->
                        </nav>
                    </div>
                </div>
            </div>

<?php get_footer(); ?>
