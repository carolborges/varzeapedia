<?php get_header(); ?>
            <div class="container">
                <div class="col-md-9 pull-left box-titulo-pagina">
					<?php if ( have_posts() ) : the_post(); ?>
                    <h1><?php the_title(); ?></h1>
                    <ol class="breadcrumb">
                      <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                      <li>
                  			<a href="<?php echo get_site_url();  ?>/noticias">
          				  		<?php $nomeTipoPost = get_post_type_object( 'noticias' );
					     		     	echo $nomeTipoPost->labels->name; ?>
						        	</a>
						        </li>
                      <li class="active"><?php the_title(); ?></li>
                    </ol>
                    <div class="col-md-12 box-conteudo-pagina">
          <!--             <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" class="pull-right">
 -->
                     	<?php the_content(); ?>

                        <div class="box-comments-post">
                            <h6>Deixe seu comentário</h6>
                            <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="600" data-numposts="3" data-colorscheme="light"></div>
                        </div>
                    </div>
                <?php endif; ?>
                </div>
                <div class="col-md-3 pull-right sidebar">
                    <h6>LEIA TAMBÉM</h6>
                    <ul class="lista-leia-tbm">
                        <?php  $args =  array('post_type' => 'noticias', 'posts_per_page'=>12, 'order'=>'DESC');
                         query_posts( $args );
                        ?>
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                        <li>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>

                         <?php endwhile; ?>

                    </ul>
                </div>
            </div>
        </div>
<?php get_footer(); ?>
